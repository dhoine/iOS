//
//  UIImageView+getImageFromUrl.swift
//  Marvel
//
//  Created by Максим on 08.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

extension UIImageView {
    
    func getImageWithUrl(_ request : AnyObject?, url: String, complition : @escaping (_ downloadedImageUrl : String, _ image : UIImage?) -> Void) -> URLSessionTask? {
        let inFileSystem = findImageInFileSystem(url).0
        let fileName = findImageInFileSystem(url).1
        guard inFileSystem else {
            let request = downloadimageWithURL(fileName: fileName, imageURL: url, completion: { (error, imageURL) in
                guard error == nil else {
                    print("No Image Available")
                    complition(imageURL, nil)
                    return
                }
                _ = self.getImageFromFileSystem(url, complition: { (imageData, imageUrl, filePath) in
                    if imageData != nil {
                        let image = UIImage(data: imageData!)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setBackgroundNotification"), object: nil, userInfo: ["image" : image!, "imageUrl" : imageUrl])
                        complition(imageURL, image)
                    } else {
                        print("No Image Available")
                        complition(imageURL, nil)
                    }
                })
            })
            return request
        }
        _ = self.getImageFromFileSystem(url, complition: { (imageData, imageUrl, filePath) in
            if imageData != nil {
                let image = UIImage(data: imageData!)
                NotificationCenter.default.post(name: NSNotification.Name("setBackgroundNotification"), object: nil, userInfo: ["image": image!, "imageURL" : imageUrl])
                complition(imageUrl, image)
            } else {
                print("No Image Available")
                complition(imageUrl, nil)
            }
        })
        return request as? URLSessionTask
    }
    
    // MARK: - Find Image From File System
    
    fileprivate func findImageInFileSystem(_ url: String) -> (Bool, String) {
        let fileManager = FileManager()
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = URL(string: paths[0])
        let dataPath = documentsDirectory?.appendingPathComponent("images")
        let cataloguePath = dataPath?.absoluteString
        if !fileManager.fileExists(atPath: cataloguePath!) {
            do {
                try fileManager.createDirectory(atPath: cataloguePath!, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        let utf8str = url.data(using: String.Encoding.utf8)
        let base64Encoded = utf8str!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let datapathURL = URL(string: cataloguePath!)
        let filePath = datapathURL?.appendingPathComponent(base64Encoded+".jpg")
        return (fileManager.fileExists(atPath: filePath!.absoluteString), base64Encoded)
    }
    
    // MARK: - Get Image From File System
    
    func getImageFromFileSystem(_ url : String, complition : (_ imageData : Data?, _ imageUrl : String, _ filePath: URL?) -> Void) {
        
        let fileManager = FileManager()
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = URL(string: paths[0])
        let dataPath = documentsDirectory?.appendingPathComponent("images")
        let utf8str = url.data(using: String.Encoding.utf8)
        let base64Encoded = utf8str!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let URLbase64Encoded = URL(string: base64Encoded)
        let filePath = dataPath?.appendingPathComponent(base64Encoded+".jpg")
        
        if fileManager.fileExists(atPath: (filePath?.absoluteString)!) {
            let data = try? Data(contentsOf: URL(fileURLWithPath: (filePath?.absoluteString)!))
            complition(data, url, filePath)
        } else {
            complition(nil, url, URLbase64Encoded)
        }
    }
    
    // MARK: - Download Image With URL
    
    fileprivate func downloadimageWithURL(fileName: String?, imageURL: String, completion: @escaping (_ error : Error?, _ imageURL: String) -> Void) -> URLSessionTask {
        
        let url = NSURL(string: imageURL) as! URL
        let dataTask = URLSession.shared.dataTask(with: url) {
            data, response, error in
            if error == nil {
                if let data = data {
                    let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
                    let documentsDirectory = URL(string: paths[0])
                    let dataPath = documentsDirectory?.appendingPathComponent("images")
                    let cataloguePath = dataPath
                    let filePath = cataloguePath?.appendingPathComponent(fileName!+".jpg")
                    let data : NSData = data as NSData
                    data.write(toFile: (filePath?.absoluteString)!, atomically: true)
                    completion(nil, imageURL)
                }
            }
        }
        return dataTask
    }
    
}


