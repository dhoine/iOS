//
//  CharacterCVCC.swift
//  Marvel
//
//  Created by Максим on 08.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import UIKit

import Alamofire

class CharacterCVCC: UICollectionViewCell {
    
    // MARK: - PublicAPI
    
    var character : Character! {
        didSet {
            self.updateUI()
        }
    }
    
   fileprivate var imageView = UIImageView()
    
    var task : URLSessionTask?
        
    // MARK: - Private
    
    @IBOutlet weak var characterImageView: UIImageView!
    @IBOutlet weak var characterNameLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var textView: UIView!
    
    
    fileprivate func updateUI() {
        var requestUrl: String = ""
        TRequestManager.sharedInstance.concurrentQueue.sync{
            requestUrl = self.character.getFullImageUrl(variantImageName: VariantImageName.portrait_uncanny)
            self.characterNameLabel?.text = self.character.name
        }
        
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            self.characterImageView?.alpha = 0.0
            
        }
        
        task = self.imageView.getImageWithUrl(nil, url: requestUrl, complition: { (downloadedImageWithUrl, image) in
            if downloadedImageWithUrl == requestUrl {
                DispatchQueue.main.async(execute: {
                    self.characterImageView.image = image
                    self.characterImageView?.alpha = 1.0
                    self.activityIndicatorView.stopAnimating()
                })
            }
        })
    }
    
    func pauseDownloadImage() {
        task?.cancel()
    }
    
    func resumeDownloadImage() {
        task?.resume()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 60.0
        self.clipsToBounds = true
        
    }
}







