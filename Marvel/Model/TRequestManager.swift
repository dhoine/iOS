//
//  TRequestManager.swift
//  Marvel
//
//  Created by Максим on 05.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//
import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class TRequestManager
{
    // Singleton
    static let sharedInstance = TRequestManager()
    
    let concurrentQueue = DispatchQueue(label: "realmQueue", attributes: .concurrent)
    
    func getRequestWithURL<T: Mappable>(_ requestURL : String?, offset: Int, count: Int, completion : @escaping (_ elements : [T]?, _ error : Error?) -> Void) {
        if let requestURL = requestURL {
           let url = Urls.baseURL + "/" + requestURL
            _ = Alamofire.request(url,
                                  method: .get,
                                  parameters: [
                                    "ts" : Urls.timestamp,
                                    "apikey" : ApiKeys.publicKey,
                                    "hash" : hash,
                                    "offset" : offset,
                                    "limit" : count])
                
                .responseArray(keyPath: "data.results", completionHandler: { (response: DataResponse<[T]>) in
                    guard response.result.isSuccess else {
                       print("Wooops something gone wrong. Error : \(response.result.error)")
                       completion(nil, response.result.error)
                       return
                    }
                    let elements = response.result.value
                    completion(elements, nil)
                })
        }
    }
    
    fileprivate var hash: String! {
        get{
            return makeHash(Urls.timestamp, publicKey: ApiKeys.publicKey, privateKey: ApiKeys.privateKey)
        }
    }
    
    fileprivate func makeHash(_ timestamp: Int, publicKey: String, privateKey: String) -> String {
        return "\(timestamp)\(privateKey)\(publicKey)".md5()
    }
}

struct Urls {
    static let baseURL = "http://gateway.marvel.com"
    static let characterUrl = "v1/public/characters"
    static let timestamp = 1
}

private struct ApiKeys {
    static let publicKey = "44c6606f72114e4a9eaf84cb93fb8863"
    static let privateKey = "a92b6b3b8140846adf4eeebb64d576b5570be727"
}

struct VariantImageName {
    static let portrait_small = "portrait_small"           // 50x75px
    static let portrait_medium = "portrait_medium"         // 100x150px
    static let portrait_xlarge = "portrait_xlarge"         // 150x225px
    static let portrait_fantastic = "portrait_fantastic"   // 168x252px
    static let portrait_uncanny = "portrait_uncanny"       // 300x450px
    static let portrait_incredible = "portrait_incredible" // 216x324px
}
