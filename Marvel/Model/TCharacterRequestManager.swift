//
//  TCharacterRequestManager.swift
//  Marvel
//
//  Created by Максим on 05.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class TCharacterRequestManager {
    
    func getCharactersWithResponse(offset: Int, count: Int, completion: @escaping(_ characters: [Character]?, _ error: NSError?) -> Void) {
        TRequestManager.sharedInstance.getRequestWithURL(Urls.characterUrl, offset: offset, count: count) { (elements: [Character]?, error) in
            guard error == nil else {
                print("Something gone wrong. Error : \(error)")
                completion(nil, error as NSError?)
                return
            }
            
            // Saved data to Realm
//            print(Realm.Configuration.defaultConfiguration.fileURL)
            if let elements = elements {
                
                TRequestManager.sharedInstance.concurrentQueue.sync{
                    let realm = try! Realm()
                    
                    try! realm.write({
                        realm.add(elements, update: true)
                    })
                    completion(elements, nil)
                }
            }
        }
    }
    
}
