//
//  Events.swift
//  Marvel
//
//  Created by Максим on 06.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Events: Object, Mappable {
    
    dynamic var available = Int()
    dynamic var collectionURI = ""
    let eventsContext = LinkingObjects(fromType: Character.self, property: "events")
    var eventsItems = List<EventsItem>()
    
    required convenience init?(map: Map) {
        self.init()
    }
}

extension Events {
    
    func mapping(map: Map) {
        available <- map["available"]
        collectionURI <- map["collectionURI"]
        eventsItems <- map["items"]
    }
}
