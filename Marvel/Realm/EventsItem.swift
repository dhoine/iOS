//
//  EventsItem.swift
//  Marvel
//
//  Created by Максим on 06.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class EventsItem: Object, Mappable {
    
    dynamic var name = ""
    dynamic var resourceURI = ""
    let eventItem = LinkingObjects(fromType: Events.self, property: "eventsItems")
    
    required convenience init?(map: Map) {
        self.init()
    }
}

extension EventsItem {
    
    func mapping(map: Map) {
        name <- map["name"]
        resourceURI <- map["resourceURI"]
    }
}
