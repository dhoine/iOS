//
//  StoriesIrem.swift
//  Marvel
//
//  Created by Максим on 06.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class StoriesItem: Object, Mappable {
    
    dynamic var name = ""
    dynamic var resourceURI = ""
    dynamic var type = ""
    let storiesItem = LinkingObjects(fromType: Stories.self, property: "storiesItems")
    
    required convenience init?(map: Map) {
        self.init()
    }
}

extension StoriesItem {
    
    func mapping(map: Map) {
        name <- map["name"]
        resourceURI <- map["resourceURI"]
        type <- map["type"]
    }
}
