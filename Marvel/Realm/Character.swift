//
//  Character.swift
//  Marvel
//
//  Created by Максим on 06.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper


class Character: Object, Mappable {
    
    dynamic var uid = Int()
    dynamic var name = ""
    dynamic var desc = ""
    dynamic var modified = ""
    dynamic var resourceURI = ""
    dynamic var comics : Comics?
    dynamic var events : Events?
    dynamic var image : Thumbnail?
    dynamic var series : Series?
    dynamic var stories : Stories?
    var urls = List<URLS>()
    
    override static func primaryKey() -> String? {
        return "uid"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
}


extension Character {
    
    func mapping(map: Map) {
        
        uid <- map["id"]
        name <- map["name"]
        desc <- map["description"]
        modified <- map["modified"]
        resourceURI <- map["resourceURI"]
        comics <- map["comics"]
        events <- map["events"]
        image <- map["thumbnail"]
        series <- map["series"]
        stories <- map["stories"]
        urls <- map["urls"]
    }
    
    func getFullImageUrl(variantImageName: String) -> String {
        if self.image?.path != nil , self.image?.ext != nil {
            let fullURLForCharacterImage = self.image!.path + "/" + variantImageName + "." + self.image!.ext
            return fullURLForCharacterImage
        } else {
            return ""
        }
    }
}



