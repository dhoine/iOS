//
//  ComicsItem.swift
//  Marvel
//
//  Created by Максим on 06.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class ComicsItem: Object, Mappable {
    
    dynamic var name = ""
    dynamic var resourceURI = ""
    let comicsItem = LinkingObjects(fromType: Comics.self, property: "comicsItems")
    
    required convenience init?(map: Map) {
        self.init()
    }
}

extension ComicsItem {
    
    func mapping(map: Map) {
        name <- map["name"]
        resourceURI <- map["resourceURI"]
    }
}
