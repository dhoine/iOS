//
//  Stories.swift
//  Marvel
//
//  Created by Максим on 06.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Stories: Object, Mappable {
    
    dynamic var available = Int()
    dynamic var collectionURI = ""
    let storiesContext = LinkingObjects(fromType: Character.self, property: "stories")
    var storiesItems = List<StoriesItem>()
    
    required convenience init?(map: Map) {
        self.init()
    }
}

extension Stories {
    
    func mapping(map: Map) {
        available <- map["available"]
        collectionURI <- map["collectionURI"]
        storiesItems <- map["items"]
    }
}
