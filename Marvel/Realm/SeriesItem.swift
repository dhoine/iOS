//
//  SeriesItem.swift
//  Marvel
//
//  Created by Максим on 06.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class SeriesItem: Object {
    
    dynamic var name = ""
    dynamic var resourceURI = ""
    let seriesItem = LinkingObjects(fromType: Series.self, property: "seriesItems")
    
    required convenience init?(map: Map) {
        self.init()
    }
}

extension SeriesItem {
    
    func mapping(map: Map) {
        name <- map["name"]
        resourceURI <- map["resourceURI"]
    }
}
